--[[

     Licensed under GNU General Public License v2
      * (c) 2013,      Luke Bonham
      * (c) 2010-2012, Peter Hofmann
      * (c) 2017,      Ado

--]]

local helpers  = require("lain.helpers")
local wibox    = require("wibox")
local math     = { ceil   = math.ceil }
local string   = { format = string.format,
                   gmatch = string.gmatch }
local tostring = tostring

-- wireless signal quality
-- lain.widget.wlan

local function factory(args)
    local wlan     = { widget = wibox.widget.textbox() }
    local args     = args or {}
    local wlanfile = "/proc/net/wireless"
    local wlanex   = "wlp2s0: "
    local timeout  = args.timeout or 2
    local settings = args.settings or function() end

    function wlan.update()
        local times = helpers.lines_match(wlanex, wlanfile)

	if next(times) ~= nil then
		for i, j in pairs(times) do
			el = tostring(j)
		end

		local quality = tonumber(el:sub(15, 17))*100/70
		quality = quality - quality % 1

		wlan_quality = tostring(quality) .. "% "
	else
		wlan_quality = "OFF"
	end

        widget = wlan.widget

        settings()
    end

    helpers.newtimer("wlan", timeout, wlan.update)

    return wlan
end

return factory
